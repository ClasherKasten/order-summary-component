module.exports = {
	content: ["./src/**/*.{html,js,css}"],
	theme: {
		extend: {
			fontFamily: {
				"rhd": ["Red Hat Display"]
			},
			screens: {
				"bgm-break": "376px",
				"bgd-break": "1441px"
			},
			colors: {
				"os-pale-blue": "hsl(225, 100%, 94%)",
				"os-bright-blue": "hsl(245, 75%, 52%)",
				"os-very-pale-blue": "hsl(225, 100%, 98%)",
				"os-desaturated-blue": "hsl(224, 23%, 55%)",
				"os-desaturated-blue-50": "hsla(224, 23%, 55%, 0.05)",
				"os-dark-blue": "hsl(223, 47%, 23%)",
				"os-dark-blue-faded": "hsl(244, 82%, 68%)",
			}
		},
	},
	plugins: [],
}
